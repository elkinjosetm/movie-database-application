//Check for browser support
if (typeof ( Storage ) !== "undefined")
{    
    //Define all the procedures for MovieController
    app.controller( 'MovieController', function ( $scope, $state, $stateParams )
    {
        var id = $stateParams.movieId;
        
        //Get Movie object
        $scope.movie = $scope.getMovieById( id );
        
        //Generate Movie rating
        $scope.generateMovieRating( id );
        
        //Generate Related Actor temporary table for realtime binding
        $scope.generateRelatedActors( id );
        
        //Set newRate to 0
        $scope.newRate = 0;
        
        /*
         * Method trigged when click non related actor
         */
        $scope.joinActor = function ( actorId )
        {
            //Create a new actor - movie relationship
            $scope.insertActorsInMovie( actorId, $scope.movie.id );
            
            //Set Related Actor temporary table for realtime binding
            $scope.generateRelatedActors( id );
            
            //Close current modal
            $( '#actorSelector' ).modal( 'hide' );            
        };
        
        /*
         * 
         */
        $scope.updateRate = function ()
        {   
            $scope.insertMovieRate( $scope.movie.id, $scope.newRate );
            
            //Generate Movie rating
            $scope.generateMovieRating( id );
            
            $scope.newRate = 0;
        };
        
        //customOptions configuration for current controller
        $scope.customOptions.length = 0;
        $scope.customOptions.push({
            icon:   'trash',
            name:   'Delete',
            url:    $state.href('deleteMovie', { movieId: id }),
            class:  'danger'
        });
    });
}