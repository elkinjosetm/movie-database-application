//Check for browser support
if (typeof ( Storage ) !== "undefined")
{    
    //Define all the procedures for MainController
    app.controller( 'HomeController', function ( $scope, $state, $stateParams )
    {   
        $scope.customOptions.length = 0;
        $scope.customOptions.push(
                {
                    icon: 'plus',
                    name: 'Movie',
                    dataToggle: 'modal',
                    dataTarget: '#addMovie'
                });
        
    });
}