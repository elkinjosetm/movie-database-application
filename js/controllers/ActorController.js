//Check for browser support
if (typeof ( Storage ) !== "undefined")
{    
    //Define all the procedures for MainController
    app.controller( 'ActorController', function ( $scope, $state, $stateParams )
    {
        var id = $stateParams.actorId;
        
        //Get Movie object
        $scope.actor = $scope.getActorById( id );
        
        //Get Actor of the current Movie
        $scope.movies = $scope.getMoviesByActorId( id );
        
        $scope.customOptions.length = 0;
        $scope.customOptions.push({
            icon:   'trash',
            name:   'Delete',
            url:    $state.href('deleteActor', { actorId: id }),
            class:  'danger'
        });
    });
}