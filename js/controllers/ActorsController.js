//Check for browser support
if (typeof ( Storage ) !== "undefined")
{    
    //Define all the procedures for MainController
    app.controller( 'ActorsController', function ( $scope, $state, $stateParams )
    {
        $scope.customOptions.length = 0;
        $scope.customOptions.push({
                    icon: 'plus',
                    name: 'Actor',
                    url: '',
                    dataToggle: 'modal',
                    dataTarget: '#addActor'
                });
        
    });
}