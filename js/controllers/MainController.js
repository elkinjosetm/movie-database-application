//Check for browser support
if (typeof ( Storage ) !== "undefined")
{    
    /*
     * MainController is the primary controller of the, wher defines the functionalities that are able thru the app,
     * for example, the appName or the navbar options like addMovie.
     * 
     * @param {type} param
     */
    app.controller( 'MainController', function ( $scope, $state ,$stateParams )
    {   
        var $this = this;
        
        //Load demo data, for the first use of the app
        $scope.loadData();
        
        $scope.appName = 'Movie Library';
        
        $scope.navbarLinks = [
            {
                icon:       'home',
                name:       'Home',
                url:        $state.href( "home" ),
                dataToggle: '',
                dataTarget: ''
            },
            {
                icon:       '',
                name:       'Movies',
                url:        $state.href( "movies" ),
                dataToggle: '',
                dataTarget: ''
            },
            {
                icon:       '',
                name:       'Actors',
                url:        $state.href( "actors" ),
                dataToggle: '',
                dataTarget: ''
            },
            {
                icon:       'remove',
                name:       'Reset LocalStorage',
                url:        $state.href( "resetStorage" ),
                dataToggle: '',
                dataTarget: ''
            }
        ];
        
        //Custom navbar options
        $scope.customOptions = [];        
        
        /*
         * Reason of this...
         * 
         * The next methods shuldn't be here, but as we use a _common partialView to implement navrbars, and other
         * HTML element, these elements need the next method to get working
         */
        
        /*
         * addActor function
         * 
         * Build a new Actor Object
         * 
         * @returns {undefined}
         */
        $this.addActor = function ()
        {
            var newActor = {
                id:         $scope.$storage.currentIds.actor++,
                firstName:  $this.firstName,
                lastName:   $this.lastName,
                birthDate:  $this.birthDate,
                gender:     $this.gender,
                image:      $this.image
            };
            
            $scope.insertActor( newActor );
            
            //When this method is trigged from a diferent controller, $stateParams should have the movieId setted
            if ( $stateParams.movieId )
            {
                $scope.generateRelatedActors( $stateParams.movieId );
            }
            
            $( '#addActor' ).modal( 'hide' );
            $( '#addActor form' ).trigger("reset");
        };
        
        /*
         * addMovie function
         * 
         * Build a new Movie Object, and send to insertMovie function to be saved.
         * 
         * @returns {undefined}
         */
        $this.addMovie = function ()
        {
            var newMovie = {
                id:             $scope.$storage.currentIds.movie++,
                name:           $this.movieName,
                releaseYear:    $this.releaseYear,
                grossIncome:    $this.grossIncome,
                directorName:   $this.directorName,
                genre:          $this.genre,
                cover:          $this.cover,
                actors:         $this.actors
            };
            
            $scope.insertMovie( newMovie );
            
            $( '#addMovie' ).modal( 'hide' );
            $( '#addMovie form' ).trigger("reset");
        };
        
        //Search onSelect event function
        $this.autocompleteOnSelect = function ( selected )
        {
            switch ( selected.type )
            {
                case 'actor':
                    $state.go( 'viewActor', { actorId: selected.id } );
                    break;
                case 'movie':
                    $state.go( 'viewMovie', { movieId: selected.id } );
                    break;
            }
        };
    });
};