//Check for browser support
if (typeof (Storage) !== "undefined")
{  
    // angularJS variable
    var app = angular.module('movieApp', ['ngStorage', 'ui.router']);
    
    /*
     * Global functions abailables before the controllers implementation, like application persistence
     */
    app.run(function ( $rootScope, $localStorage )
    {
        //Initial app variables
        $rootScope.$storage = null;
        
        //Arrays used to monitor the current movie-actors relationship in "realtime"
        $rootScope._relatedActors = [];
        $rootScope._notRelatedActors = [];
        
        //Rating temp variable
        $rootScope._rating = 0;
        
        //Empty array for all the autoComplete information
        $rootScope._autoCompleteData = [];
        
        /*
         * loadData
         * 
         * This function put in the local storage all needed information for the app run with usable information for DEMO
         * 
         * @returns {undefined}
         */
        $rootScope.loadData = function ()
        {
            //Check if demo data is loaded in localStorage
            if ( ! $localStorage.demoData )
            {
                //Upload demo information
                $rootScope.$storage = $localStorage.$default({
                    demoData:       true,
                    movies:         demoMovies,
                    ratings:        demoRatings,
                    actors:         demoActors,
                    actorsInMovies: demoActorsInMovies,
                    currentIds:     demoCurrentIds
                });
            }
            else {
                //Load localStorage
                $rootScope.$storage = $localStorage;
            }
            
            //Generate autocompleteData based on recent changes
            $rootScope.generateAutocompleteData();
        };

        /*
         * resetLocalStorage
         * 
         * This function reset the local storage, and delete everything. WARNING!!!!
         * 
         * @returns {undefined}
         */
        $rootScope.resetLocalStorage = function ()
        {
            //Reset Localstorage
            $localStorage.$reset();
            
            //Reload All Demo data
            $rootScope.loadData();
        };
        
        /*
         * getMovieById
         * 
         * Search an movie by Id
         * 
         * @param {integer} id
         * @returns {object}
         */
        $rootScope.getMovieById = function ( id )
        {
            return $rootScope.getElementById( $rootScope.$storage.movies, id );
        };
        
        /*
         * generateMovieRating
         * 
         * @param {type} id
         * @returns {unresolved}
         */
        $rootScope.generateMovieRating = function ( id )
        {
            //Rating variables
            var rating = 0, ratings = $rootScope.getMovieRatings( id );
            
            angular.forEach( ratings, function ( value, key ){
                rating = rating + value.rating;
            });
            
            $rootScope._rating = 0;
            $rootScope._rating = (rating / ratings.length);
        };
        
        
        /*
         * getMovieRatings
         * 
         * @param {type} id
         * @returns {unresolved}
         */
        $rootScope.getMovieRatings = function ( id )
        {   
            return $.grep($rootScope.$storage.ratings, function ( object )
            {
                return object.movieId === parseInt( id );
            });
        };
        
        /*
         * getActorById
         * 
         * Search an actor by Id
         * 
         * @param {integer} id
         * @returns {object}
         */
        $rootScope.getActorById = function ( id )
        {
            return $rootScope.getElementById( $rootScope.$storage.actors, id );
        };

        /*
         * getActorsByMovie
         * 
         * Search the actors by Movie
         * 
         * @param {integer} movieId
         * @returns {object}
         */
        $rootScope.getActorsByMovieId = function ( movieId )
        {   
            var result, relationship = [];
            
            //Get related actors to the movie passed
            result = $.grep($rootScope.$storage.actorsInMovies, function ( object )
            {
                return object.movieId === parseInt( movieId );
            });
            
            //Build the mapped object
            angular.forEach(result, function (value, key)
            {
                var actor = $rootScope.getActorById(value.actorId);
                
                try
                {
                    actor.relationshipId = value.id;
                    relationship.push( actor );
                }
                catch (error)
                {
                    console.log(error);
                }
            });

            return relationship;
        };

        /*
         * getNotRelatedActorsByMovieId
         * 
         * Search the actors that aren't in the selected movie
         * 
         * @param {integer} movieId
         * @returns {object}
         */
        $rootScope.getNotRelatedActorsByMovieId = function ( movieId )
        {   
            var relatedActors = [],     //Related Actors array
                notRelatedActors = [];  //Not related Actors array
                
            relatedActors = $rootScope.getActorsByMovieId( movieId );

            //Loop for checking which actor is not related to current movie
            angular.forEach($rootScope.$storage.actors, function (value, key)
            {
                //Check if selected actor is in relatedActors array. If not, push to notRelatedActors array
                if ( relatedActors.indexOf(value) === -1 )
                {
                    //
                    notRelatedActors.push(value);
                }
            });
            
            return notRelatedActors;
        };
        
        /*
         * generateRelatedActors
         * 
         * Set related actors temp table for "realtime" binding
         * 
         * @param {integer} movieId
         * @returns {object}
         */
        $rootScope.generateRelatedActors = function ( movieId )
        {
            //Set temp localStorage table, for real time binding
            $rootScope._relatedActors = $rootScope.getActorsByMovieId( movieId );
            
            //Call to update Not Realted temp table
            $rootScope.generateNotRelatedActors( movieId );
        };

        /*
         * generateNotRelatedActors
         * 
         * set not related actors in temp local table, for "realtime" binding
         * 
         * @param {integer} movieId
         * @returns {object}
         */
        $rootScope.generateNotRelatedActors = function ( movieId )
        {   
            //Set temp localStorage table, for real time binding
            $rootScope._notRelatedActors = $rootScope.getNotRelatedActorsByMovieId( movieId );
        };
        
        /*
         * getMoviesByActorId
         * 
         * Search the Movies by Actor
         * 
         * @param {integer} actorId
         * @returns {object}
         */
        $rootScope.getMoviesByActorId = function ( actorId )
        {   
            var result, relationship = [];
            
            //Get related actors to the movie passed
            result = $.grep($rootScope.$storage.actorsInMovies, function ( object )
            {
                return object.actorId === parseInt( actorId );
            });
            
            //Build the mapped object
            angular.forEach(result, function(value, key)
            {
                var movie = $rootScope.getMovieById(value.movieId);
                movie.relationshipId = value.id;
                relationship.push( movie );
            });

            return relationship;
        };
        
        /*
         * getElementById
         * 
         * Search an element by Id in objectArray
         * 
         * @param {object} objectArray
         * @param {integer} id
         * @returns {object}
         */
        $rootScope.getElementById = function ( objectArray, id )
        {
            return $.grep(objectArray, function ( object )
            {
                return object.id === parseInt( id );
            })[0];
        };

        /*
         * insertMovie
         * 
         * Insert Movie in LocalStorage
         * 
         * @param {type} newMovie
         */
        $rootScope.insertMovie = function ( newMovie )
        {   
            //Insert the new movie in localStorage
            $rootScope.$storage.movies.push( newMovie );
            
            //Insert the 0 rate to the new movie
            $rootScope.insertMovieRate( newMovie.id, 0 );
            
            angular.forEach(newMovie.actors, function (value, key){
                $rootScope.insertActorsInMovie( value, newMovie.id );
            });
            
            //Generate autocompleteData based on recent changes
            $rootScope.generateAutocompleteData();
        };

        /*
         * insertMovieRate
         * 
         * Insert movie Rate in LocalStorage
         * 
         * @param {type} idMovie
         * @param {type} rate
         */
        $rootScope.insertMovieRate = function ( idMovie, rate )
        {   
            $rootScope.$storage.ratings.push( { id: $rootScope.$storage.currentIds.rating++, movieId: idMovie, rating: parseFloat(rate) } );
        };

        /*
         * insertActor
         * 
         * Insert Actor in LocalStorage
         * 
         * @param {type} newActor
         */
        $rootScope.insertActor = function ( newActor )
        {
            $rootScope.$storage.actors.push( newActor );
            
            //Generate autocompleteData based on recent changes
            $rootScope.generateAutocompleteData();
        };

        /*
         * insertActorsInMovie
         * 
         * Insert Actors vs Movies relationship in LocalStorage
         * 
         * @param {type} newActor
         */
        $rootScope.insertActorsInMovie = function ( actorId, movieId )
        {
            $rootScope.$storage.actorsInMovies.push({
                id: $rootScope.$storage.currentIds.actorsInMovie++,
                actorId: actorId,
                movieId: movieId
            });
        };
        
        /*
         * deleteMovieById
         * 
         * Delete Movie from LocalStorage
         * 
         * @param {type} movieId
         */
        $rootScope.deleteMovieById = function ( movieId )
        {
            var relatedActors   = $rootScope.getActorsByMovieId( movieId ),
                ratings         = $rootScope.getMovieRatings( movieId );
            
            angular.forEach(relatedActors, function(value, key)
            {
                $rootScope.deleteActorMovieRelationship( value.relationshipId );
            });
            
            angular.forEach(ratings, function(value, key)
            {
                $rootScope.deleteElementById( $rootScope.$storage.ratings, value.id );
            });
            
            $rootScope.$storage.movies = $rootScope.deleteElementById( $rootScope.$storage.movies, movieId );
        };
        
        /*
         * deleteActorById
         * 
         * Delete Actor from LocalStorage
         * 
         * @param {type} actorId
         */
        $rootScope.deleteActorById = function ( actorId )
        {
            var relatedMovies = $rootScope.getMoviesByActorId( actorId );
              
            angular.forEach(relatedMovies, function(value, key)
            {
                $rootScope.deleteActorMovieRelationship( value.relationshipId );
            });
            
            $rootScope.$storage.actors = $rootScope.deleteElementById( $rootScope.$storage.actors, actorId );
        };
        
        
        /*
         * deleteActorMovieRelationship
         * 
         * Delete Actor Movie Relationship from LocalStorage
         * 
         * @param {type} actorId
         */
        $rootScope.deleteActorMovieRelationship = function ( id )
        {
            $rootScope.$storage.actorsInMovies= $rootScope.deleteElementById( $rootScope.$storage.actorsInMovies, id );
        };
         
        /*
         * deleteElementById
         * 
         * Delete Element from LocalStorage
         * 
         * @param {type} movieId
         */
        $rootScope.deleteElementById = function ( objectArray, id )
        {
            var object = $rootScope.getElementById( objectArray, id );
            objectArray.splice( objectArray.indexOf(object), 1 );
            
            //Generate autocompleteData based on recent changes
            $rootScope.generateAutocompleteData();
            
            return objectArray;
        };
        
        /*
         * generateAutocompleteData
         * 
         * This is the method to generate the autocomplete data
         */
        $rootScope.generateAutocompleteData = function ()
        {
            //Autocomplete data
            $rootScope._autoCompleteData = [];

            //Include all movies into autocompleteData array
            angular.forEach($rootScope.$storage.movies, function (value, key){
                $rootScope._autoCompleteData.push({
                    id:     value.id,
                    type:   'movie',
                    name:   value.name,
                    image:  value.cover
                });
            });

            //Include all actors into autocompleteData array
            angular.forEach($rootScope.$storage.actors, function (value, key){
                $rootScope._autoCompleteData.push({
                    id:     value.id,
                    type:   'actor',
                    name:   value.firstName + ' ' +  value.lastName,
                    image:  value.image
                });
            });
        };
    });
}
else
{
    alert('Sorry. Web Storage is not supported by your browser...');
}