// Demo information
var demoMovies = [
    {
        id: 1,
        name: 'Furious Seven',
        releaseYear: '2015',
        grossIncome: '100000000',
        directorName: 'James Wan',
        genre: 'Action, Crime',
        cover: 'https://s.ynet.io/assets/images/movies/furious_seven_2015/medium-cover.jpg'
    },
    {
        id: 2,
        name: 'San Andreas',
        releaseYear: '2015',
        grossIncome: '20000000',
        directorName: 'Brad Peyton',
        genre: 'Action, Drama',
        cover: 'https://s.ynet.io/assets/images/movies/san_andreas_2015/medium-cover.jpg'
    }
];

var demoRatings = [
    {
        id:         1,
        movieId:    1,
        rating:     2
    },
    {
        id:         2,
        movieId:    1,
        rating:     1
    },
    {
        id:         3,
        movieId:    1,
        rating:     5
    },
    {
        id:         4,
        movieId:    2,
        rating:     5
    },
    {
        id:         5,
        movieId:    2,
        rating:     0
    }
];

var demoActors = [
    {
        id: 1,
        firstName: 'Vin',
        lastName: 'Diesel',
        gender: 'Male',
        birthDate: '1900-01-01T05:00:00.000Z',
        image: 'http://vakeromagazine.com/site/wp-content/uploads/2015/03/vin_diesel_a_p.jpg'
    },
    {
        id: 2,
        firstName: 'Paul',
        lastName: 'Walker',
        gender: 'Male',
        birthDate: '1900-01-01T05:00:00.000Z',
        image: 'http://us.emedemujer.com/wp-content/uploads/sites/3/2015/06/paul-walker-1.jpg'
    },
    {
        id: 3,
        firstName: 'Dwayne',
        lastName: 'Johnson',
        gender: 'Male',
        birthDate: '1900-01-01T05:00:00.000Z',
        image: 'https://upload.wikimedia.org/wikipedia/commons/6/68/Dwayne_Johnson_at_the_2009_Tribeca_Film_Festival.jpg'
    },
    {
        id: 4,
        firstName: 'Jason',
        lastName: 'Statham',
        gender: 'Male',
        birthDate: '1900-01-01T05:00:00.000Z',
        image: 'http://media2.popsugar-assets.com/files/2011/03/12/4/192/1922398/fa9e135c04ca9384_jason/i/Jason-Statham.jpg'
    },
    {
        id: 5,
        firstName: 'Carla',
        lastName: 'Gugino',
        gender: 'Female',
        birthDate: '1900-01-01T05:00:00.000Z',
        image: 'http://vignette4.wikia.nocookie.net/doblaje/images/2/2d/Carla-gugino-pose-f2795.jpg/revision/latest?cb=20111021000741&path-prefix=es'
    },
    {
        id: 6,
        firstName: 'Alexandra',
        lastName: 'Daddario',
        gender: 'Female',
        birthDate: '1900-01-01T05:00:00.000Z',
        image: 'https://em.wattpad.com/0d768341543b512daa0b99479c9dfd51023adc8a/687474703a2f2f63656c65626d616669612e636f6d2f77702d636f6e74656e742f75706c6f6164732f323031342f30382f616c6578616e6472612d646164646172696f2d70686f746f73686f6f742d62792d672e2d63617465732d323031342d5f332e6a7067'
    }
];

var demoActorsInMovies = [
    {
        id: 1,
        actorId: 1,
        movieId: 1
    },
    {
        id: 2,
        actorId: 2,
        movieId: 1
    },
    {
        id: 3,
        actorId: 3,
        movieId: 1
    },
    {
        id: 4,
        actorId: 4,
        movieId: 1
    },
    {
        id: 5,
        actorId: 3,
        movieId: 2
    },
    {
        id: 6,
        actorId: 5,
        movieId: 2
    },
    {
        id: 7,
        actorId: 6,
        movieId: 2
    }
];

var demoCurrentIds = {
    actor: 7,
    movie: 3,
    actorsInMovie: 8,
    rating: 6
};