//Check for browser support
if (typeof (Storage) !== "undefined")
{ 
    /*
     * reverse Filter
     * 
     * This is a custom filter used to render the array in reverse mode if it's necessary
     * 
     */
    app.filter('reverse', function ()
    {
        return function (items) {
            return items.slice().reverse();
        };
    });
};