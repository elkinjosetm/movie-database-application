//Check for browser support
if (typeof (Storage) !== "undefined")
{
    /*
     * Routing Configuration
     * 
     * Defines Application configuration
     * 
     * @param {type} param
     */
    app.config(function ($stateProvider, $urlRouterProvider)
    {
        // For any unmatched url, redirect to /state1
        $urlRouterProvider.otherwise("/");
        //
        // Now set up the states
        $stateProvider
                .state('home', {
                    url: "/",
                    templateUrl: "templates/home.html",
                    controller: 'HomeController as homeCtrl'
                })
                .state('movies', {
                    url: "/movies",
                    templateUrl: "templates/movies.html",
                    controller: 'HomeController as homeCtrl'
                })
                .state('actors', {
                    url: "/actors",
                    templateUrl: "templates/actors.html",
                    controller: 'ActorsController'
                })
                .state('viewMovie', {
                    url: "/movie/:movieId",
                    templateUrl: "templates/movie.html",
                    controller: 'MovieController'
                })
                .state('viewActor', {
                    url: "/actor/:actorId",
                    templateUrl: "templates/actor.html",
                    controller: 'ActorController'
                })
                .state('deleteMovie', {
                    url: "/deleteMovie/:movieId",
                    controller: function ($scope, $state, $stateParams){
                        $scope.deleteMovieById( $stateParams.movieId );
                        $state.go('movies');
                    }
                })
                .state('deleteActor', {
                    url: "/deleteActor/:actorId",
                    controller: function ($scope, $state, $stateParams) {
                        $scope.deleteActorById( $stateParams.actorId );
                        $state.go('actors');
                    }
                })
                .state('deleteRelationship', {
                    url: "/deleteRelationship/:relationshipId/returnTo/:movieId",
                    controller: function ($scope, $state, $stateParams) {
                        $scope.deleteActorMovieRelationship( $stateParams.relationshipId );
                        $state.go('viewMovie', { movieId: $stateParams.movieId } );
                    }
                })
                .state('resetStorage', {
                    url: "/resetStorage",
                    controller: function ($scope, $state, $stateParams) {
                        $scope.resetLocalStorage();
                        $state.go('home');
                    }
                });
    });
}
;